
import itertools as it

DATA_FILE = 'multiplexer_{}.data'
NAMES_FILE = 'multiplexer_{}.names'

def generate_multiplexer(l):

    n = l + 2**l

    n_rules = 2**(l+1)

    lst = map(list, it.product([0, 1], repeat=n))
    
    rules = []
    for ls in lst:
        pos = int(''.join(map(str, ls[:l])), 2)
        if ls[(l + pos)] == 0:
            ls.append(0)
        else:
            ls.append(1)
            

        rules.append(ls)

    return rules


def write_to_file(filename, rules):

    with open(filename, 'w') as fd:
        for r in rules:
            fd.write(",".join(map(str, r)))
            fd.write("\n")

def write_names_file(filename, n):

    with open(filename, 'w') as fd:

        fd.write('class.\t\t\t\t| target attribute\n\n')

        for c in range(n):
            fd.write('x_{}:\t\t\t\t0, 1.\n'.format(c))

        fd.write('\nclass:\t\t\t\t0, 1.\n')





def main():

    l = 3
    rules = generate_multiplexer(l)

    n = l + 2**l

    write_to_file(DATA_FILE.format(n), rules)

    write_names_file(NAMES_FILE.format(n), n)



if __name__ == '__main__':
    main()
